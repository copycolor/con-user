import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import { environment } from '../environments/environment';
import  { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { ProductsComponent } from './components/Products/Products.component';

import { ProductService } from './services/Product.service';
import { AddProductComponent } from './components/add-Product/add-Product.component';

import { FormsModule } from '@angular/forms';

import {AccordionModule} from 'primeng/accordion'; 
import {MenuItem} from 'primeng/api'; 
import {DataTableModule} from 'primeng/datatable';
import {CarouselModule} from 'primeng/carousel';
import {DataListModule} from 'primeng/datalist';
import {DataViewModule} from 'primeng/dataview';
import {TabViewModule} from 'primeng/tabview';
import {FileUploadModule} from 'primeng/fileupload';
import {PickListModule} from 'primeng/picklist';
import {OrderListModule} from 'primeng/orderlist';
import {MenubarModule} from 'primeng/menubar';
import { ListaComponent } from './components/lista/lista.component';
import {DialogModule} from 'primeng/dialog';
import { TablaComponent } from './components/tabla/tabla.component';
import {CalendarModule} from 'primeng/calendar';
import { HomeComponent } from './components/home/home.component';



import { AppRoutingModule } from './app-routing.module';

///// Start FireStarter

// Core
import { CoreModule } from './core/core.module';

// Shared/Widget
import { SharedModule } from './shared/shared.module';

// Feature Modules
import { ItemModule } from './items/shared/item.module';
import { UploadModule } from './uploads/shared/upload.module';
import { UiModule } from './ui/shared/ui.module';
import { NotesModule } from './notes/notes.module';
///// End FireStarter

export const firebaseConfig = environment.firebaseConfig;
@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    AddProductComponent,
    ListaComponent,
    TablaComponent,
    HomeComponent,
    
 
  ],
  imports: [
    BrowserModule,
    AngularFirestoreModule,
    FormsModule,
    DataTableModule,
    CarouselModule,
    DataListModule,
    DataViewModule,
    TabViewModule,
    FileUploadModule,
    PickListModule,
    OrderListModule,
    MenubarModule,
    DialogModule,
    BrowserAnimationsModule,
    CalendarModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    CoreModule,
    SharedModule,
    ItemModule,
    UiModule,
    NotesModule,
    AngularFireModule.initializeApp(firebaseConfig),
   
  ],
  providers: [
    ProductService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
