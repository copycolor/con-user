import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { ProductService } from '../../services/Product.service';
import { Product } from '../../models/Product';

@Component({
  selector: 'app-add-Product',
  templateUrl: './add-Product.component.html',
  styleUrls: ['./add-Product.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AddProductComponent implements OnInit {
  Product: Product = {
   name: '',
   place: '',
   cname: '',
   cphone: '',
   need: '',
   date: '',
   
  };
 
  constructor(private ProductService: ProductService) { }

  ngOnInit() {
  }

  onSubmit() {
    if(this.Product.name != '' 
    && this.Product.place != '' 
    && this.Product.cname != ''
    && this.Product.cphone != '' 
    && this.Product.need != ''
    && this.Product.date != '')
    
    {
      this.ProductService.addProduct(this.Product);
      this.Product.name = '';
      this.Product.place = '';
      this.Product.cname = '';
      this.Product.cphone = '';
      this.Product.need = '';
      this.Product.date = '';
     
    }

  }



  today: number = Date.now();

}
