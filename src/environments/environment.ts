// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.


export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDC7QFPEXVKQiFQWKDoAddC-UxZ-A0EQy0",
    authDomain: "copycolor-1729c.firebaseapp.com",
    databaseURL: "https://copycolor-1729c.firebaseio.com",
    projectId: "copycolor-1729c",
    storageBucket: "gs://copycolor-1729c.appspot.com/",
    messagingSenderId: "891936853879"
  }
};
